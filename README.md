nsa-quality-midlet
==================

This project contains the Java ME part to run a network quality measurement with
a Nokia 6210 classic and the standard Java platform. 

How to build the project
------------------------

The project is built with ant using [antenna](http://sourceforge.net/projects/antenna/files/).
Download antenna and put the JAR into the `lib` folder of your ant installation. Under Linux this
is usually `~/.ant/lib`.

Next you have to download and install [version 3.0.5 of the Java ME SDK](http://www.oracle.com/technetwork/java/javame/javamobile/download/sdk/java-me-sdk-3-0-5-download-1849683.html).
You need to create a file called build.user.xml in the root directory of this repository that defines the `${wtk.home}` property.

After that just run `ant` and look for the JAD/JAR files in the `dist` directory.


Example build.user.xml
------------------------

  <!DOCTYPE project>
	<project name="nsa-mobile-user">
		<property name="wtk.home" value="/home/username/sdk-3.0.5"/>
	</project>


SDK installation under Linux
------------------------

If you want to use the SDK under another platform than Windows, you also need to download the Linux version of the
[Sun Java Wireless Toolkit 2.5.2_01 for CLDC](http://www.oracle.com/technetwork/java/download-135801.html).
Then follow these steps to install the SDK (assuming POSIX-compatible shell):

	mkdir sdk-3.0.5 && cd sdk-3.0.5
	unzip -j ../oracle_java_me_sdk-3_0_5.exe data/javame-toolkit/3.0.0.0.1112132031/windows/data/data,1.jar
	unzip data,1.jar lib/* docs/* legal/* apps/*
	for i in $(ls lib/*.gz); do unpack200 -r $i $i.jar; done
	rename -v 's/\.pack\.gz\.jar//' lib/*.jar
	unzip ../sun_java_wireless_toolkit-2.5.2_01-linuxi486.bin.sh bin/* wtklib/*
	rm data,1.jar

If you are using a 64-bit system, you also need to install the relevant i386 libraries:

	sudo aptitude install libxt6:i386 libstdc++6:i386

You are now using the libraries and documentation from the Java ME SDK 3.0.5 and the binary tools (I have only tested
preverify so far) from 2.5.2_01.

Nokia Drivers
------------------------
To correctly interact with the mobile device, you have to install it's driver.
For Windows this is the Nokia PC Suite and for Linux you have to input the following command

	sudo modprobe usbserial vendor=0x421 product=0x01fe

How to develop the project
------------------------
When including the project into Eclipse or a different IDE, only the following Libaries are required in the classpath.

	<wtk.home>/lib/cldc_1.1.jar
  	<wtk.home>/lib/jsr257_1.0.jar
  	<wtk.home>/lib/midp_2.1.jar

IMPORTANT! Do not include your standard Java SDK into the classpath
