package at.ac.tuwien.nsa.qualityMidlet;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

import at.ac.tuwien.nsa.logging.LogUtils;
import at.ac.tuwien.nsa.usb.USBConnectionThread;


public class NsaQuality extends MIDlet {

	private Form form = new Form("Network Quality Measurement");

	private USBConnectionThread usbConnectionThread;
	public static int interations = 25;
	public static String url = "http://nq.codecurry.cc/nq/_design/nq/_show/random";

	public NsaQuality() {
		super();
	}

	protected void startApp() throws MIDletStateChangeException {
		Display.getDisplay(this).setCurrent(form);
		LogUtils.setForm(form);
		
		form.addCommand(new Command("Logging", Command.STOP, 1));
		form.addCommand(new Command("Clear", Command.BACK, 1));
		form.setCommandListener(new CommandListener() {

			public void commandAction(Command command, Displayable arg1) {
				if (command.getLabel().equals("Logging")) {
					LogUtils.writeLine("Logging toggled.");
					LogUtils.toggleLogging();
					LogUtils.writeLine("Logging toggled.");
				} else if (command.getLabel().equals("Clear")) {
					LogUtils.clearScreen();
				}
			}
			
		});
		
		usbConnectionThread = new USBConnectionThread();
		try {
			usbConnectionThread.start();
		} catch (Exception e) {
			LogUtils.logException(e.getClass().getName(), e);
		}
		
		LogUtils.writeLine("Waiting for command to start measurement...");
	}

	/**
	 * Closes open connections and removes listeners.
	 */
	protected void destroyApp(boolean arg0) throws MIDletStateChangeException {
		usbConnectionThread.stop();
	}

	/**
	 * Not used
	 */
	protected void pauseApp() {}

}
