package at.ac.tuwien.nsa.logging;

import javax.microedition.lcdui.Font;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;

public final class LogUtils {

	private static Form outputForm;
	private static boolean active = false;

	public static void setForm(Form form) {
		outputForm = form;
	}

	private LogUtils() {

	}

	/**
	 * Logs the Exception to the given form
	 */
	public static synchronized void logException(String msgPrefix, Exception e) {
		if (outputForm == null)
			return;
		writeLine(msgPrefix + ": " + e.getMessage());
		writeLine("ExceptionClass: " + e.getClass().getName());
		e.printStackTrace();
	}

	/**
	 * Appends the given Message in a new Line to the Form
	 */
	public static synchronized void writeLine(String message) {
		if (!active)
			return;

		String label = "Message";
		int index = message.indexOf('(');
		if (index != -1) {
			label = message.substring(0, index);
			message = message.substring(index);
		}

		StringItem si = new StringItem(label, message);
		si.setFont(Font.getFont(Font.FACE_MONOSPACE, Font.STYLE_PLAIN, Font.SIZE_SMALL));

		if (outputForm == null)
			return;
		outputForm.append(si);
	}

	public static void toggleLogging() {
		LogUtils.active = !(LogUtils.active);
	}

	public static void clearScreen() {
		outputForm.deleteAll();
	}

	public static void writeHex(byte[] message) {
		LogUtils.writeLine(bytesToHex(message));
	}

	protected static String bytesToHex(byte[] bytes) {
		final char[] hexArray = {
			'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
		};
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for (int j = 0; j < bytes.length; j++) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
