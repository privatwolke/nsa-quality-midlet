package at.ac.tuwien.nsa.protocol;

/**
 * A ResetReceivedException is thrown whenever one party receives a reset command from the other
 * party.
 */
public class ResetReceivedException extends Exception {

	public ResetReceivedException(String message) {
		super(message);
	}

}
