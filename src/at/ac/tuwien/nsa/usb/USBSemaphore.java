package at.ac.tuwien.nsa.usb;

public class USBSemaphore {

	boolean isWaiting;

	public synchronized void waitForConnectionLoss() {
		isWaiting = true;
		while (isWaiting) {
			try {
				wait(5000);
			} catch (InterruptedException e) {}
		}
	}

	public synchronized void wakeUpConnectionThread() {
		while (!isWaiting) {
			try {
				wait(100);
			} catch (InterruptedException e) {}
		}
		isWaiting = false;
		notifyAll();
	}

	public synchronized void forceWakeUpConnectionThread() {
		notifyAll();
	}

}
