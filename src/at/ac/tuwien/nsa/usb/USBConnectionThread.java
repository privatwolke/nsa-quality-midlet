package at.ac.tuwien.nsa.usb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.microedition.io.CommConnection;
import javax.microedition.io.Connector;

import at.ac.tuwien.nsa.logging.LogUtils;

public class USBConnectionThread implements Runnable {

	// TODO resolve this name dynamically
	private static final String USB_PORT_NAME = "comm:USB1";

	private Thread usbConnectionThread;
	private USBCommunicationThread communicationThread;
	private USBSemaphore usbSemaphore;
	private InputStream is;
	private OutputStream os;
	private CommConnection comm;

	public USBConnectionThread() {}

	public void run() {
		while (true) {
			closeConnections();
			LogUtils.writeLine("Waiting for cable on USB port...");
			try {
				comm = (CommConnection) Connector.open(USB_PORT_NAME);
				os = comm.openOutputStream();
				is = comm.openInputStream();
				LogUtils.writeLine("USB Connection established.");
				communicationThread = new USBCommunicationThread(is, os, usbSemaphore);
				communicationThread.start();
				usbSemaphore.waitForConnectionLoss();
			} catch (IOException e) {
				LogUtils.logException("IOException", e);
			} finally {
				closeConnections();
			}
		}
	}

	/**
	 * Checks preconditions and starts execution
	 */
	public void start() throws Exception {
		checkIfUSBPortAvailable();
		usbSemaphore = new USBSemaphore();
		if (usbConnectionThread == null) {
			usbConnectionThread = new Thread(this);
		}
		usbConnectionThread.start();
	}

	/**
	 * Stops the Thread and closes possible open connections.
	 */
	public void stop() {
		closeConnections();
		communicationThread.stop();
		usbConnectionThread = null;
		if (usbSemaphore != null) {
			usbSemaphore.forceWakeUpConnectionThread();
		}
	}

	/**
	 * Closes possible open connections.
	 */
	private void closeConnections() {
		try {
			if (os != null)
				os.close();
			if (is != null)
				is.close();
			if (comm != null)
				comm.close();
		} catch (Exception e) {}
	}

	/**
	 * Throws an exception if no USB port is available
	 */
	private void checkIfUSBPortAvailable() throws Exception {
		String ports = System.getProperty("microedition.commports");
		LogUtils.writeLine("Found ports " + ports);
		int index = ports.indexOf("USB", 0);
		if (index == -1) {
			throw new Exception("No USB port found in the device");
		}
	}

}
