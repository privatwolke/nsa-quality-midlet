package at.ac.tuwien.nsa.usb;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Random;

import javax.microedition.io.Connector;
import javax.microedition.io.HttpConnection;

import at.ac.tuwien.nsa.logging.LogUtils;
import at.ac.tuwien.nsa.protocol.AttentionMessage;
import at.ac.tuwien.nsa.protocol.HashResponse;
import at.ac.tuwien.nsa.protocol.Message;
import at.ac.tuwien.nsa.protocol.Message.ActionIdentifiers;
import at.ac.tuwien.nsa.protocol.ProtocolException;
import at.ac.tuwien.nsa.protocol.ResetReceivedException;
import at.ac.tuwien.nsa.protocol.Result;
import at.ac.tuwien.nsa.qualityMidlet.NsaQuality;

public class USBCommunicationThread implements Runnable {

	private Thread usbCommunicationThread;
	private USBSemaphore usbSemaphore;
	private InputStream is;
	private OutputStream os;

	public USBCommunicationThread(InputStream is, OutputStream os,
			USBSemaphore usbSemaphore) {
		this.is = is;
		this.os = os;
		this.usbSemaphore = usbSemaphore;
	}

	public void run() {
		LogUtils.writeLine("Listening on USB port...");

		short readLength = 4;
		boolean waiting = true;

		while (true) {
			try {

				if (waiting) {
					AttentionMessage attn = new AttentionMessage(is);
					if (attn.getNextCommandLength() != 0) {
						readLength = attn.getNextCommandLength();
						waiting = false;
					}
					HashResponse hr = new HashResponse(attn);
					os.write(hr.getBytes());
					LogUtils.writeLine(hr.toString());
					LogUtils.writeLine(attn.toString());
				} else {
					Message request = new Message(is, readLength);
					LogUtils.writeLine(request.toString());

					HashResponse hr = new HashResponse(request);
					LogUtils.writeLine(hr.toString());
					os.write(hr.getBytes());

					Message result = null;

					switch (request.getActionIdentifier()) {
					case ActionIdentifiers.START_EVAL_UP:
						result = runUploads();
						break;
					case ActionIdentifiers.START_EVAL_DOWN:
						result = runDownloads();
						break;
					}
					if (result == null) {
						os.write(new Result(false).getBytes());
					} else {
						os.write(result.getBytes());
					}
					waiting = true;
				}
			} catch (IOException e) {
				LogUtils.logException(
						"IOException occured, closing connection", e);
				break;
			} catch (ProtocolException e) {
				LogUtils.logException(
						"ProtocolException occured, reseting Communication", e);
				try {
					os.write(Message.RESET.getBytes());
				} catch (IOException e1) {
					// ignore since by now, everything has gone wrong
				}
				break;
			} catch (ResetReceivedException e) {
				LogUtils.logException(
						"Reset message received, reseting Communication", e);
				break;
			}
		}
		LogUtils.writeLine("USB connection lost.");
		closeConnections();
		usbSemaphore.wakeUpConnectionThread();
	}

	public void start() {
		if (usbCommunicationThread == null) {
			usbCommunicationThread = new Thread(this);
		}
		usbCommunicationThread.start();
	}

	/**
	 * Stops the Thread and closes possible open connections.
	 */
	public void stop() {
		closeConnections();
		usbCommunicationThread = null;
	}

	/**
	 * Closes possible open connections.
	 */
	private void closeConnections() {
		try {
			if (os != null)
				os.close();
			if (is != null)
				is.close();
		} catch (Exception e) {
		}
	}

	private Message runDownloads() throws IOException {
		LogUtils.writeLine("Performing initial Dummy Download.");
		performUpload();
		LogUtils.writeLine("Performing Downloads...");
		for (int index = 0; index < NsaQuality.interations; index++) {
			os.write(new Result(true).getBytes());
			performDownload();
			os.write(new Result(true).getBytes());
		}
		LogUtils.writeLine("Finished Downloads...");
		return Message.EVAL_FINISHED;
	}

	private Message runUploads() throws IOException {
		LogUtils.writeLine("Performing initial Dummy Upload.");
		performUpload();
		LogUtils.writeLine("Performing Uploads...");
		for (int index = 0; index < NsaQuality.interations; index++) {
			os.write(new Result(true).getBytes());
			performUpload();
			os.write(new Result(true).getBytes());
		}
		LogUtils.writeLine("Finished Uploads...");
		return Message.EVAL_FINISHED;
	}

	private void performUpload() throws IOException {
		OutputStream os = null;
		HttpConnection c = null;
		try {
			c = (HttpConnection) Connector.open(NsaQuality.url);
			c.setRequestMethod(HttpConnection.POST);
			os = c.openOutputStream();
			os.write(getRandom5KBytes());
			LogUtils.writeLine("ResponseCode "+c.getResponseCode());
			os.close();
		} finally {
			os.close();
			c.close();
		}
	}

	private byte[] getRandom5KBytes() {
		byte[] fiveKBytes = new byte[5000];
		Random random = new Random(Calendar.getInstance().getTime().getTime());
		for (int index = 0; index < 5000; index++) {
			fiveKBytes[index] = (byte) random.nextInt();
		}
		return fiveKBytes;
	}

	private void performDownload() throws IOException {
		StringBuffer b = new StringBuffer();
		InputStream is = null;
		HttpConnection c = null;

		try {
			long len = 0;
			int ch = 0;
			c = (HttpConnection) Connector.open(NsaQuality.url);
			is = c.openInputStream();
			len = c.getLength();

			if (len != -1) {
				// Read exactly Content-Length bytes
				for (int i = 0; i < len; i++) {
					if ((ch = is.read()) != -1) {
						b.append((char) ch);
					}
				}
			} else {
				// Read till the connection is closed.
				while ((ch = is.read()) != -1) {
					len = is.available();
					b.append((char) ch);
				}
			}
		} finally {
			is.close();
			c.close();
		}

	}

}
